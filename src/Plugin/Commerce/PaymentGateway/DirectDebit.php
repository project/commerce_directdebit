<?php

namespace Drupal\commerce_directdebit\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use PHP_IBAN\IBAN;

/**
 * Provides the Direct Debit payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "direct_debit",
 *   label = "Direct Debit",
 *   display_label = "Direct Debit",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_directdebit\PluginForm\DirectDebitAddForm",
 *   },
 *   payment_method_types = {"direct_debit_sepa", "direct_debit_uk"},
 * )
 */
class DirectDebit extends OnsitePaymentGatewayBase implements DirectDebitInterface {

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    if (!$capture) {
      // Only capture transaction type is currently supported.
      throw new HardDeclineException('The payment was declined');
    }

    $next_state = $capture ? 'completed' : 'authorization';

    $payment->setState($next_state);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    self::assertKeyIsInPaymentDetails($payment_details, 'accept_direct_debits');
    self::assertKeyIsInPaymentDetails($payment_details, 'account_name');
    self::assertKeyIsInPaymentDetails($payment_details, 'debit_date');
    self::assertKeyIsInPaymentDetails($payment_details, 'one_signatory');

    $payment_method->accept_direct_debits = trim($payment_details['accept_direct_debits']);
    $payment_method->account_name = trim($payment_details['account_name']);
    $payment_method->debit_date = trim($payment_details['debit_date']);
    $payment_method->one_signatory = trim($payment_details['one_signatory']);

    switch ($payment_method->bundle()) {
      case 'direct_debit_sepa':
        self::assertKeyIsInPaymentDetails($payment_details, 'iban');
        self::assertKeyIsInPaymentDetails($payment_details, 'swift');

        $payment_method->iban = trim($payment_details['iban']);
        $payment_method->swift = trim($payment_details['swift']);
        break;

      case 'direct_debit_uk':
        self::assertKeyIsInPaymentDetails($payment_details, 'account_number');
        self::assertKeyIsInPaymentDetails($payment_details, 'sort_code');

        $payment_method->account_number = trim($payment_details['account_number']);
        $payment_method->sort_code = trim($payment_details['sort_code']);
        break;

      default:
        break;
    }

    // Keys specific validations.
    switch ($payment_method->bundle()) {
      case 'direct_debit_sepa':
        // Validate BIC number.
        if (!preg_match("/^([a-zA-Z]){4}([a-zA-Z]){2}([0-9a-zA-Z]){2}([0-9a-zA-Z]{3})?$/", $payment_method->swift->value)) {
          throw new DeclineException('Invalid BIC number.');
        }

        // Validate IBAN number.
        $iban = new IBAN($payment_method->iban->value);
        if (!$iban->Verify()) {
          throw new DeclineException('Invalid IBAN number.');
        }

        break;

      case 'direct_debit_uk':
        // Validate sort code.
        if (strlen(preg_replace('/[^0-9]+/', '', $payment_method->sort_code->value)) < 6) {
          throw new DeclineException('Invalid sort code - must contain 6 numbers.');
        }

        // Validate account number.
        if (strlen(preg_replace('/[^0-9]+/', '', $payment_method->account_number->value)) < 8) {
          throw new DeclineException('Invalid account number - must contain 8 numbers.');
        }

        break;

      default:
        break;
    }

    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
  }

  private static function assertKeyIsInPaymentDetails(array $payment_details, string $required_key): void {
    if (!array_key_exists($required_key, $payment_details)) {
      throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
    }
  }

}
