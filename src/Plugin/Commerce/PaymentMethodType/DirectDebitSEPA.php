<?php

namespace Drupal\commerce_directdebit\Plugin\Commerce\PaymentMethodType;

use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the Direct Debit SEPA payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "direct_debit_sepa",
 *   label = @Translation("Direct Debit SEPA"),
 * )
 */
class DirectDebitSEPA extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Direct Debit SEPA');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['account_name'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE);

    $fields['swift'] = BundleFieldDefinition::create('string')
      ->setLabel(t('BIC number'))
      ->setRequired(TRUE);

    $fields['iban'] = BundleFieldDefinition::create('string')
      ->setLabel(t('IBAN number'))
      ->setRequired(TRUE);

    $fields['debit_date'] = BundleFieldDefinition::create('integer')
      ->setLabel(t('Debit date'))
      ->setRequired(TRUE)
      ->setSetting('size', 'tiny');

    $fields['accept_direct_debits'] = BundleFieldDefinition::create('boolean')
      ->setLabel(t('Does it accept Direct Debits?'))
      ->setRequired(TRUE);

    $fields['one_signatory'] = BundleFieldDefinition::create('boolean')
      ->setLabel(t('Is it one signatory account?'))
      ->setRequired(TRUE);

    return $fields;
  }

}
