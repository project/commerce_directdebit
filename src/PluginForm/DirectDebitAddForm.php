<?php

namespace Drupal\commerce_directdebit\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class DirectDebitAddForm extends PaymentMethodAddForm {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    switch ($payment_method->bundle()) {
      case 'direct_debit_sepa':
        $form['payment_details'] = $this->buildDirectDebitSepaForm($payment_method, $form_state);
        break;

      case 'direct_debit_uk':
        $form['payment_details'] = $this->buildDirectDebitUkForm($payment_method, $form_state);
        break;

      default:
        break;
    }

    return $form;
  }

  /**
   * Builds the direct debit SEPA form.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built credit card form.
   */
  protected function buildDirectDebitSepaForm(PaymentMethodInterface $payment_method, FormStateInterface $form_state) {
    $element = [];

    $element['#attributes']['class'][] = 'direct-debit-sepa-form';
    $element['account_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bank account name'),
      '#default_value' => $payment_method->get('account_name')->value,
    ];

    $element['swift'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BIC number'),
      '#default_value' => $payment_method->get('swift')->value,
    ];

    $element['iban'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IBAN number'),
      '#default_value' => $payment_method->get('iban')->value,
    ];

    $element['debit_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Debit date'),
      '#default_value' => $payment_method->get('debit_date')->value,
    ];

    $element['accept_direct_debits'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Does it accept Direct Debits?'),
      '#default_value' => $payment_method->get('accept_direct_debits')->value,
    ];

    $element['one_signatory'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is it one signatory account?'),
      '#default_value' => $payment_method->get('one_signatory')->value,
    ];

    return $element;
  }

  /**
   * Builds the direct debit SEPA form.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built credit card form.
   */
  protected function buildDirectDebitUkForm(PaymentMethodInterface $payment_method, FormStateInterface $form_state) {
    $element = [];

    $element['#attributes']['class'][] = 'direct-debit-sepa-form';
    $element['account_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bank account name'),
      '#default_value' => $payment_method->get('account_name')->value,
    ];

    $element['sort_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bank sort code'),
      '#default_value' => $payment_method->get('sort_code')->value,
    ];

    $element['account_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bank account number'),
      '#default_value' => $payment_method->get('account_number')->value,
    ];

    $element['debit_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Debit date'),
      '#default_value' => $payment_method->get('debit_date')->value,
    ];

    $element['accept_direct_debits'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Does it accept Direct Debits?'),
      '#default_value' => $payment_method->get('accept_direct_debits')->value,
    ];

    $element['one_signatory'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is it one signatory account?'),
      '#default_value' => $payment_method->get('one_signatory')->value,
    ];

    return $element;
  }

}
